$(document).ready(function(){
    //set terms to local storage

    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    });
    
    $('.term-add-div').on('click','#term-add-close', function(e) {
        $('#term-add-close').attr('id','term-add').html('<i class="fas fa-plus fa-fw"></i>');
        $('#termInput').fadeOut('slow', function () {
            $('#termInput').remove();
        });
        console.log('close');
        
    });
    
    $('#term-add').on('click',function(e) {
        $('.term-add-div').html('<input type="text" class="term-add-text" id="termInput" />');
        $('#termInput').css('display', 'none');
        $('#termInput').fadeIn();
        $('#term-add').attr('id','term-add-close').html('<i class="fas fa-minus fa-fw"></i>');
        console.log('add');
    });

    var i = 0;
    for (i = 0; i < localStorage.length; i++) {
        var termID = "term-" + i;
        $('#termList').append("<span class='term' id='" + termID + "' data-toggle='tooltip' data-placement='bottom' title='Right click to remove'>" + localStorage.getItem(localStorage.key(i)) + "</span>");
    }

    $('#clear').click(function () {
        localStorage.clear();
    });
    
    $('.term-add-div').on("submit", function () {
        if ($('#termInput').val() !== "") {
            var termID = "term-" + i;
            var termMessage = $('#termInput').val();
            localStorage.setItem(termID, termMessage);
            $('#termList').append("<span class='term' id='" + termID + "' data-toggle='tooltip' data-placement='bottom' title='Right click to remove'>" + termMessage + "</span>");
            var term = $('#' + termID);
            term.css('display', 'none');
            term.fadeIn();
            $('#termInput').remove();
            i++;
        }
        return false;
    });

    $('#termList').on("contextmenu", "span.term", function (event) {
        event.preventDefault();
        self = $(this);
        termID = self.attr('id');
        localStorage.removeItem(termID);
        self.fadeOut('slow', function () {
            self.remove();
        });

    });

    $('#termList').on('click', '.term', function(e){
        var add = $(this).text();
        $('input[name="q"]').val($('input[name="q"]').val() + add+' ');
        $('input[name="q"]').focus();
    });
});