$(document).ready(function(){
    //set terms to local storage
    function makeid() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 5; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    }

    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    });

    if(localStorage.length !== 0) {
        $.each(localStorage, function (key, value) {
            if (/^term/.test(key)) {
                $('#termList').append("<span class='term' id='" + key + "' data-toggle='tooltip' data-placement='bottom' title='Right click to remove'>" + value + "</span>");
            }
        });
    }else{
        $('#termList').append('<span class="term-before">Click <i class="fas fa-plus-circle"></i> to a add new search prefix</span>');
    }


    $('#add-hide').on('click', function(){
        $( "#termInput" ).animate({'width': 'toggle'},1000);
    });

    $('.term-add-form').on("submit", function () {
        if ($('#termInput').val() !== "") {
            $('span.term-before').remove();
            var termID = "term-" + makeid();
            var termMessage = $('#termInput').val();
            localStorage.setItem(termID, termMessage);
            $('#termList').append("<span class='term' id='" + termID + "' data-toggle='tooltip' data-placement='bottom' title='Right click to remove'>" + termMessage + "</span>");
            var term = $('#' + termID);
            term.css('display', 'none');
            term.fadeIn();
            $('#termInput').remove();
            i++;
        }
        return false;
    });

    $('#termList').on("contextmenu", "span.term", function (event) {
        event.preventDefault();
        self = $(this);
        termID = self.attr('id');
        localStorage.removeItem(termID);
        self.fadeOut('slow', function () {
            self.remove();
        });
        if(localStorage.length == 0) {
            $('#termList').append('<span class="term-before">Click <i class="fas fa-plus-circle"></i> to a add new search prefix</span>');
        }
    });

    $('#termList').on('click', '.term', function(e){
        var add = $(this).text();
        $('input[name="q"]').val($('input[name="q"]').val() + add+' ');
        $('input[name="q"]').focus();
    });

});